//
//  SubscriptionPlansViewController.swift
//  musicplayer3_tabs
//
//  Created by Kindlebit on 13/07/18.
//  Copyright © 2018 storyboard. All rights reserved.
//

import UIKit
import SwiftyStoreKit
import StoreKit
import AFNetworking


class SubscriptionPlansViewController: UIViewController {
    
    var productIdentifier = "storyboard_subscription" //Get it from iTunes connect
    var productId = "storyboard_subscription"
    var productsRequest = SKProductsRequest()
    var iapProducts = [SKProduct]()
    var subscriptionPurchaseMade = UserDefaults.standard.bool(forKey: "subscriptionPurchaseMade")
    let appBundleId = "com.storyboardapp"
    var isPremium : Bool = false
    var activity_indicator = UIActivityIndicatorView()
    var defaults = UserDefaults.standard
   
    @IBOutlet weak var myLabel: UILabel!
    @IBOutlet weak var myCustomView: UIView!
    @IBOutlet weak var buyButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.myCustomView.layer.borderWidth = 1
        self.myCustomView.layer.borderColor = UIColor.black.cgColor
        
        fetchAvailableProducts()
        // Do any additional setup after loading the view.
    }
  
    @IBAction func tapButtonPressed(_ sender: UIButton){
        let myController = storyboard?.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
        self.navigationController?.pushViewController(myController, animated: true)
    }
    func fetchAvailableProducts()
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        SwiftyStoreKit.retrieveProductsInfo(["storyboard_subscription"]) { (result) in
            if let product = result.retrievedProducts.first
            {
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                self.iapProducts = [product]
                let priceString = product.localizedPrice!
                print("Product: \(product.localizedDescription), price: \(priceString)")
            }
            else if let invalidProductId = result.invalidProductIDs.first {
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                print("Invalid product identifier: \(invalidProductId)")
            }
            else {
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                print("Error: \(result.error)")
            }
        }
    }
    func verifyReciept()
    {
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: "09656a477a86416ea79363846c9cdb6f")
        SwiftyStoreKit.verifyReceipt(using: appleValidator, forceRefresh: false) { result in
            switch result {
            case .success(let receipt):
                print("Verify receipt success: \(receipt)")
                
            case .error(let error):
                print("Verify receipt failed: \(error)")
            }
        }
    }
    func fetchReciept()
    {
        SwiftyStoreKit.fetchReceipt(forceRefresh: true) { result in
            switch result {
            case .success(let receiptData):
                let encryptedReceipt = receiptData.base64EncodedString(options: [])
                self.verifyReciept()
                print("Fetch receipt success:\n\(encryptedReceipt)")
            case .error(let error):
                print("Fetch receipt failed: \(error)")
            }
        }
    }
    func purchaseSubscription()
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let productId = "storyboard_subscription"
        SwiftyStoreKit.purchaseProduct(productId, atomically: true) { result in
            
            if case .success(let purchase) = result {
                // Deliver content from server, then:
                if purchase.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
                
                let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: "09656a477a86416ea79363846c9cdb6f")
                SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
                    
                    if case .success(let receipt) = result {
                        let purchaseResult = SwiftyStoreKit.verifySubscription(
                            ofType: .autoRenewable,
                            productId: productId,
                            inReceipt: receipt)
                        
                        switch purchaseResult {
                        case .purchased(let expiryDate, let receiptItems):
                            
                            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                            if let mostRecent = receiptItems.first
                            {
                                //print("\(productId) is valid until \(mostRecent.subscriptionExpirationDate)\n\(receiptItems)\n")
                                let dateformate = DateFormatter()
                                dateformate.dateFormat = "yyyy-MM-dd HH:mm"
                                let purchaseDate = dateformate.string(from: mostRecent.purchaseDate)
                                let dateformate1 = DateFormatter()
                                dateformate1.dateFormat = "yyyy-MM-dd HH:mm"
                                let expirationDate = dateformate.string(from: mostRecent.subscriptionExpirationDate!)
                                
                                self.defaults.set("1", forKey: "Premium")
                                self.defaults.set(expirationDate, forKey: "Expiredate")
                                var canceldate = mostRecent.cancellationDate as? String
                                
                                let product = mostRecent.productId as! String
                                let transactionID = mostRecent.originalTransactionId as! String
                                if(canceldate == nil) || (canceldate == "")
                                {
                                    canceldate = ""
                                }
                                else
                                {
                                    let dateformate12 = DateFormatter()
                                    dateformate12.dateFormat = "yyyy-MM-dd HH:mm"
                                    canceldate = dateformate.string(from: (mostRecent.cancellationDate)!)
                                }
                                self.buySubscription(cancelDate: canceldate!, purchaseDate: purchaseDate, productid: product, transactionID: transactionID , expireDate : expirationDate)
                            }
                            print("Product is valid until \(expiryDate)")
                            
                        case .expired(let expiryDate, let receiptItems):
                            
                            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                            if let mostRecent = receiptItems.first
                            {
                                let dateformate1 = DateFormatter()
                                dateformate1.dateFormat = "yyyy-MM-dd HH:mm"
                                let expirationDate = dateformate1.string(from: mostRecent.subscriptionExpirationDate!)
                                
                                self.defaults.set("0", forKey: "Premium")
                                self.defaults.set(expirationDate, forKey: "Expiredate")
                            }
                           
                           
                            
                        case .notPurchased:
                            
                            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                            self.defaults.set("0", forKey: "Premium")
                            self.defaults.set("", forKey: "Expiredate")
                            print("This product has never been purchased")
                        }
                        
                    } else {
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        // receipt verification error
                    }
                }
            } else {
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                // purchase error
            }
        }
        
        
    }
    // MARK: Method used for verifying the subscription status.
    func recieptValidator()
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: "09656a477a86416ea79363846c9cdb6f")
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
            switch result {
            case .success(let receipt):
                let productId = "storyboard_subscription"
                // Verify the purchase of a Subscription
                let purchaseResult = SwiftyStoreKit.verifySubscription(
                    ofType: .autoRenewable,
                    productId: productId,
                    inReceipt: receipt)
                
                switch purchaseResult {
                case .purchased(let expiryDate, let items):
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    if let mostRecent = items.first
                    {
                        let dateformate1 = DateFormatter()
                        dateformate1.dateFormat = "yyyy-MM-dd HH:mm"
                        let expirationDate = dateformate1.string(from: mostRecent.subscriptionExpirationDate!)
                        
                        self.defaults.set("1", forKey: "Premium")
                        self.defaults.set(expirationDate, forKey: "Expiredate")
                        print("\(productId) is valid until \(String(describing: mostRecent.subscriptionExpirationDate))\n\(items)\n")
                        
                        
                    }
                    
                    
                    
                case .expired(let expiryDate, let items):
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    if let mostRecent = items.first
                    {
                        let dateformate1 = DateFormatter()
                        dateformate1.dateFormat = "yyyy-MM-dd HH:mm"
                        let expirationDate = dateformate1.string(from: mostRecent.subscriptionExpirationDate!)
                        
                        self.defaults.set("0", forKey: "Premium")
                        self.defaults.set(expirationDate, forKey: "Expiredate")
                        print("\(productId) is valid until \(mostRecent.subscriptionExpirationDate)\n\(items)\n")
                        
                    }
                    
                case .notPurchased:
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    self.purchaseSubscription()
                }
                
            case .error(let error):
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                print("Receipt verification failed: \(error)")
            }
        }
    }
    // MARK : save subscription data to backend.
    func buySubscription(cancelDate : String, purchaseDate : String, productid : String, transactionID : String, expireDate : String )
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let dateformate = DateFormatter()
        dateformate.dateFormat = "yyyy-MM-dd HH:mm"
        
        
        
        let s_date = dateformate.string(from: Date())
        print(s_date)
        
        let user_idchk = UserDefaults.standard.string(forKey: "user_id") as? String
        let URL = "\(appDelegate.common.Obj.base_url)apple_subscription.php"
        let params = ["user_id": user_idchk,
                      "subscription_product_id": productid,
                      "subscription_cancel_date": cancelDate,
                      "subscription_transaction_id": transactionID,
                      "subscription_start_date": purchaseDate,
                      "subscription_end_date": expireDate
        ]
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFJSONResponseSerializer()
        
        manager.post(URL, parameters: params, constructingBodyWith: { (form) in
            
        }, progress: { (progress) in
            
        }, success: { (operation, responseObject ) -> Void in
            
            let dict = responseObject as! NSDictionary
            print(dict)
            let code =  (dict.object(forKey: "response") as! NSDictionary).value(forKey: "code") as! String
            if code == "200"
            {
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                print("data saved succesfully")
                let appDelegat = UIApplication.shared.delegate as! AppDelegate
                let rootController : UITabBarController = appDelegat.window?.rootViewController as! UITabBarController
                let index = rootController.selectedIndex
                let target = (rootController.viewControllers as! NSArray).object(at: index) as! UINavigationController
                target.popToRootViewController(animated: false)
                rootController.selectedIndex = 1
            }
            else
            {
                // self.alertController(message: (dict.object(forKey: "response") as! NSDictionary).value(forKey: "msg") as! String, alert: "Alert")
                //                let alertController = UIAlertController.init(title: "Error!", message: "Already Subscribed.", preferredStyle: .alert)
                //                let alertAction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
                //                alertController.addAction(alertAction)
                //                self.present(alertController, animated: true, completion: nil)
                // print((dict.object(forKey: "response") as! NSDictionary).value(forKey: "msg") as! String)
            }
            
        }) { (operation, error) -> Void in
            print(error)
            //self.alertController(message: "Something is wrong!", alert: "Alert")
            
        }
    }
    @IBAction func buyButtonPressed(_ sender: UIButton)
    {
        purchaseSubscription()
    }
    
    func alertMessage(title : String , message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default , handler: nil)
        alertController.addAction(alertAction)
        self.present(alertController, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func privacyButtonPressed(_ sender: UIButton)
    {
        UIApplication.shared.openURL(NSURL(string: "http://www.trystoryboard.com/privacy")! as URL)
    }
    @IBAction func termsButtonPressed(_ sender: UIButton)
    {
        UIApplication.shared.openURL(NSURL(string: "http://www.trystoryboard.com/privacy")! as URL)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
