//
//  FirstViewController.swift
//  musicplayer3_tabs
//
//  Created by JP on 12/7/17.
//  Copyright © 2017 storyboard. All rights reserved.
//

import UIKit
import AFNetworking

class FirstViewController: UIViewController,UITableViewDelegate, UITableViewDataSource
{
    let defaults = UserDefaults.standard
    var arrData = [episode]()
    var filteredData = [episode]()
    var SortedData = [episode]()
    var arrShow = [String]()
    var showNameVariable = ""
    var cellHeight : CGFloat = 121
    
    @IBOutlet weak var viewNoData : UIView!
    @IBOutlet weak var contnView : noDataPopUP!
    
    @IBOutlet weak var Btn_emptyButton: UIButton?
    @IBOutlet weak var Btn_Search: UIButton!
    
    var show_cat = ""
    
    @IBOutlet weak var txtSearchBar : UITextField!
    @IBOutlet weak var imgTitle : UIImageView!
    
    var searchActive : Bool = false
    
    @IBOutlet weak var activity_indicator: UIActivityIndicatorView!
    @IBOutlet weak var nowPlayingImageView: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    
       
//        let arr = ["Hello","Bye","Halo"]
//         let filtered = filteredData.filter { $0.contains("lo") }
//         print(filtered)
        
        
        self.tabBarController?.tabBar.isHidden = false
        episodeTable.layoutMargins = UIEdgeInsets.zero
        episodeTable.separatorInset = UIEdgeInsets.zero
        nowPlayingImageView.imageView?.animationImages = AnimationFrames.createFrames()
        nowPlayingImageView.imageView?.animationDuration = 1.0
    
        navigationController?.isNavigationBarHidden=true
        activity_indicator.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.songpause(noti:)), name: NSNotification.Name(rawValue: "songpause"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.songPlay(noti:)), name: NSNotification.Name(rawValue: "songplay"), object: nil)
        
    }

    
    @IBAction func click_Wave(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EpisodeViewController") as! EpisodeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func songPlay(noti:Notification) {
        
        audioPlayer.play()
        startNowPlayingAnimation(true)
    }
    
    @objc func songpause(noti:Notification) {
        audioPlayer.pause()
        startNowPlayingAnimation(false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden=true
        let loginSes = userDef.value(forKey: "session")
        if(loginSes == nil){
            Btn_emptyButton?.isHidden = false
            Btn_Search.isHidden = false
            episodeTable?.isHidden = true
            self.viewNoData.isHidden = true
           // self.ApiGetUserFeeds()
        }
        else{
            let outData = userDef.data(forKey: "userData")
            let dict = NSKeyedUnarchiver.unarchiveObject(with: outData!)as! NSDictionary
            print(dict)
            
            let organisatin_id = dict.value(forKey: "organization_id") as? Int
            if(organisatin_id == nil){
                Btn_emptyButton?.isHidden = true
                Btn_Search.isHidden = true
                episodeTable?.isHidden = true
                self.viewNoData.isHidden = false
                self.contnView.lblMsg.text = "No Organisation found for this user.Please contact to provider."
                
            }else{
                 self.apiForOrganisationFeed()
                Btn_emptyButton?.isHidden = true
                Btn_Search.isHidden = true
                episodeTable?.isHidden = false
                self.viewNoData.isHidden = true
            }
        }
        
        episodeTable.tableFooterView = UIView()
        self.episodeTable.reloadData()
        arrShow = getArray()
        if (audioPlayer != nil) {
            if  audioPlayer.isPlaying {
                nowPlayingImageView.isHidden = false
                startNowPlayingAnimation(true)
            }else{
                nowPlayingImageView.isHidden = true
                startNowPlayingAnimation(false)
            }
        }
        else {
            nowPlayingImageView.isHidden = true
            startNowPlayingAnimation(false)
        }
    }
    
    @IBAction func clickOnShowPodacst(_ sender : UIButton){
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PodcastsTableViewController") as! PodcastsTableViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func apiForOrganisationFeed(){
        let manager = AFURLSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        let req: NSMutableURLRequest? = AFJSONRequestSerializer().request(withMethod: "GET", urlString: Constant.FeedUrl_Organisation, parameters: nil, error: nil)
        let uid = userDef.value(forKey: "uid") as! String
        let access_token = userDef.value(forKey: "access-token") as! String
        let client = userDef.value(forKey: "client") as! String
        req?.setValue("application/json", forHTTPHeaderField: "content-type")
        req?.setValue("\(uid)", forHTTPHeaderField: "uid")
        req?.setValue("\(access_token)", forHTTPHeaderField: "access-token")
        req?.setValue("\(client)", forHTTPHeaderField: "client")
        
        print("\(uid)-----\(access_token)----\(client)")
        
        if let aReq = req {
            (manager.dataTask(with: aReq as URLRequest, completionHandler: { response, responseObject, error in
                if error == nil {
                    if let dict = responseObject as? NSArray{
                        print(dict)
                    }
                    if  let dict = responseObject as?  NSDictionary{
                        print(dict)
                        let success = dict["success"] as! String
                        if(success == "false"){
                            let msg = dict["message"] as! String
                           // ApiResponse.alert(title: "Alert", message: msg, controller: self)
                            self.Btn_emptyButton?.isHidden = true
                            self.Btn_Search.isHidden = true
                            self.episodeTable.isHidden = true
                            self.viewNoData.isHidden = false
                            self.contnView.lblMsg.text = msg
                        }else{
                            let data =  dict.object(forKey: "data") as! [NSDictionary]
                            self.arrData.removeAll()
                            self.filteredData.removeAll()
                            if(data.count == 0){
                                self.Btn_emptyButton?.isHidden = true
                                self.Btn_Search.isHidden = true
                                self.episodeTable.isHidden = true
                                self.viewNoData.isHidden = false
                                self.contnView.lblMsg.text = "No podcast available."
                            }else{
                                self.viewNoData.isHidden = true
                                self.episodeTable.isHidden = false
                                self.Btn_emptyButton?.isHidden = true
                                self.Btn_Search.isHidden = true
                                
                                for i in 0 ..< data.count
                                {
                                    DispatchQueue.main.async()
                                        {
                                            let dict = data[i]
                                            print(dict)
                                            
                                            
                                            
                                            var episodeobj = episode()
                                            if let episode_name = dict["name"] as? String {
                                                episodeobj.name = episode_name
                                            }
                                            let audioDict = dict["audio"] as! NSDictionary
                                            if let epside_audio = audioDict["url"] as? String{
                                                episodeobj.audio = epside_audio
                                            }
                                            if let episode_date = dict["date"] as? String{
                                                episodeobj.date = episode_date
                                            }
                                            if let episode_show = dict["show_id"] as? String{
                                                episodeobj.show = episode_show
                                            }
                                            
                                            if let episode_desc = dict["description"]  as? String {
                                                episodeobj.description = episode_desc
                                            }
                                            
                                            let img = dict["image"] as! NSDictionary
                                            //  let thumb = img["thumb"] as! NSDictionary
                                            if let episode_image = img["url"]  as? String {
                                                episodeobj.image = episode_image
                                            }
                                            //                                else{
                                            //                                    episodeobj.image = "https://via.placeholder.com/150"
                                            //                                }
                                            
                                            
                                            
                                            //                                    let dateformate = DateFormatter()
                                            //                                    dateformate.dateFormat = "MM-dd-yyyy"
                                            //
                                            //                                    let e_date = dateformate.date(from: episode_date)
                                            //                                    dateformate.dateFormat = "MMMM dd, yyyy"
                                            //
                                            //                                    let s_date = dateformate.string(from: e_date!)
                                            //                                    episodeobj.date = s_date
                                            //                                    let epdate = dateformate.date(from: s_date)
                                            //                                    if epdate == nil {
                                            //                                        episodeobj.episode_date = Date()
                                            //                                    }
                                            //                                    else {
                                            //                                        episodeobj.episode_date = epdate
                                            //                                    }
                                            self.arrData.append(episodeobj)
                                            self.filteredData.append(episodeobj)
                                            
                                            
                                            self.SortedData = self.filteredData.sorted { $0.name < $1.name }
                                            
                                            self.episodeTable.reloadData()
                                    }
                                }
                            }
                            self.episodeTable.reloadData()
                        }
                    }
                } else {
                    if let anError = error, let anObject = responseObject {
                        
                        print(anObject)
                        
                        let alertController = UIAlertController.init(title: "Alert", message: "Your administrator appears to have removed this account from their organization. If you believe this was done incorrectly, please contact them to be re-added for future podcast episodes.", preferredStyle: .alert)
                        let alertAction = UIAlertAction.init(title: "OK", style: .default) { (action) in
                            
                            userDef.removeObject(forKey: "session")
                            userDef.removeObject(forKey: "userData")
                            userDef.removeObject(forKey: "organization_id")
                            isNAvigateFrom = ""
                            
                            isLoginFrom = "No_Orgn"
                            let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                            self.navigationController?.pushViewController(login, animated: true)
                            
                        }
                        alertController.addAction(alertAction)
                        self.present(alertController, animated: true, completion: nil)
                        
                        
                        
                    }
                }
            })).resume()
        }
    }
   
    @IBAction func Click_Play(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EpisodeViewController") as! EpisodeViewController
        self.navigationController?.pushViewController(vc, animated: true)
        let str = Bundle.main.path(forResource: "430824201-user-774038550-tulane-spring-game-review-ep-28", ofType: "m4a")
        nameVariableInSecondVc = "Local"
        audioVariableInSecondVc = str!
        descriptionVariable = "fdfd"
        
    }
    @IBOutlet var episodeTable: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return self.SortedData.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return cellHeight
    }
    
   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let livedata = self.SortedData[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! FirstViewCell
        cell.view_complete.isHidden = true
        cell.img_song.sd_setImage(with: URL(string: livedata.image!), completed: nil)
        
        let url_img = "\(Constant.img_baseUrl)\(livedata.image!)"
        let imageURL = URL(string: url_img)
        if  imageURL != nil {
            cell.img_song.sd_setImage(with: imageURL, placeholderImage: nil, options: .continueInBackground, completed: nil)
        } else {
            cell.img_song.image = UIImage(named: "placeholder")
        }
        
        
        print(livedata.date)
        let DateFormateChange = livedata.date.ChangeDateFormat(Date: livedata.date, fromFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSXXX", toFormat: "MM-dd-yyyy")
      
        cell.lbl_date.text = DateFormateChange
        cell.lbl_episode.numberOfLines = 2
      //  let shortData = livedata.name as NSArray = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
      // print(shortData)
        
        cell.lbl_episode.text = livedata.name
        cell.lbl_episode.sizeToFit()
        
        cell.lbl_description.numberOfLines = 2
        cell.lbl_description.sizeToFit()
        cell.lbl_description.text = livedata.description
        
        let lblS = cell.lbl_date.frame.origin.y + cell.lbl_date.frame.size.height + 10
        
        let imgS = cell.img_song.frame.origin.y + cell.img_song.frame.size.height + 10
        
        if !(lblS < imgS) {
            
            cellHeight = cell.lbl_date.frame.origin.y + cell.lbl_date.frame.size.height + 10
        }
        else {
            
            cellHeight = 121
        }
        
        cell.img_status.tag = 500

        
        // extract json audio file
        
        var url = ""
        
        if (livedata.paywall == "no") {
            cell.contentView.backgroundColor = UIColor(red:0.89, green:0.95, blue:0.99, alpha:1.0)
        } else {
            cell.contentView.backgroundColor = UIColor(red:1.00, green:0.93, blue:0.70, alpha:1.0)
        }
        
        if livedata.audio != nil {
            
            url = livedata.audio!
            
            if let audioUrl = URL(string: url) {
                
                // then lets create your document folder url
                let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                
                // lets create your destination file url
                let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
                
                // to check if it exists before downloading it
                if FileManager.default.fileExists(atPath: destinationUrl.path) {
                    print("The file already exists at path")
                    //kndl complete
                    cell.view_complete.isHidden = false
                    cell.img_status.isHidden=true
                }
                else{
                    
                    // this is the code I am testing
                    //cell.img_status.image = UIImage(named: "download.png")
                    cell.img_status.isHidden=false
                    cell.img_status.image = UIImage(named: "download")
                    cell.img_status.tag = indexPath.row
                   
                }
                
            } // end audio if
        }
        
        return cell
    }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let epi = self.SortedData[indexPath.row]
        var url = ""
        let loginSes = userDef.value(forKey: "session")
        if(loginSes == nil){
            let alert = UIAlertController(title: "Alert", message: "Please login first!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    self.navigationController?.pushViewController(login, animated: true)
                case .cancel:
                    print("cancel")
                case .destructive:
                    print("jfhjfh")
                }}))
            self.present(alert, animated: true, completion: nil)
            return
        }
        else
        {
                    if epi.audio != nil {
                        url = epi.audio!
                            if let audioUrl = URL(string: url)
                            {
                                
                                // then lets create your document folder url
                                let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                                
                                // lets create your destination file url
                                let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
                                
                                if FileManager.default.fileExists(atPath: destinationUrl.path) {
                                    
                                    print("The file already exists at path")
                                    
                                    audioPlayer = nil
                                    
                                    let when = DispatchTime.now() + 0.5
                                    
                                    DispatchQueue.main.asyncAfter(deadline: when) {
                                        
                                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EpisodeViewController") as! EpisodeViewController
                                        self.navigationController?.pushViewController(vc, animated: true)
                                        nameVariableInSecondVc = epi.name
                                        
                                        audioVariableInSecondVc =  epi.audio!
                                        showTitleVariable = self.showNameVariable
                                        descriptionVariable = epi.description
                                        imageVariable = epi.image!
                                        showDateVariable = epi.date
                                        
                                    }
                                    
                                    
                                    // if the file doesn't exist
                                } else {
                                    
                                    activity_indicator.isHidden = false
                                    activity_indicator.startAnimating()
                                    UIApplication.shared.beginIgnoringInteractionEvents()
                                    
                                    // you can use NSURLSession.sharedSession to download the data asynchronously
                                    URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                                        guard let location = location, error == nil else {
                                            
                                            return
                                            
                                        }
                                        do {
                                            // after downloading your file you need to move it to your destination url
                                            try FileManager.default.moveItem(at: location, to: destinationUrl)
                                            print("File moved to documents folder")
                                            
                                            
                                            let when = DispatchTime.now() + 0.5
                                            
                                            DispatchQueue.main.asyncAfter(deadline: when) {
                                                
                                                self.activity_indicator.isHidden = true
                                                self.activity_indicator.stopAnimating()
                                                UIApplication.shared.endIgnoringInteractionEvents()
                                                
                                                let cell = tableView.cellForRow(at: indexPath) as! FirstViewCell
                                                self.alertController(message: "Success", alert: "Download Completed.")
                                                self.episodeTable.reloadData()
                                                //cell.img_status.image = #imageLiteral(resourceName: "verification-mark")
                                                //kndl complete
                                                //cell.view_complete.isHidden = false
                                                //                                audioPlayer = nil
                                                //
                                                //                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "EpisodeViewController") as! EpisodeViewController
                                                //                                self.navigationController?.pushViewController(vc, animated: true)
                                                //                                nameVariableInSecondVc = epi.name
                                                //                                audioVariableInSecondVc = epi.audio!
                                                //                                showTitleVariable = self.showNameVariable
                                                //                                imageVariable = epi.image!
                                                
                                                print("audio:\(audioVariableInSecondVc)")
                                                
                                            }
                                            
                                            
                                        } catch let error as NSError {
                                            print(error.localizedDescription)
                                        }
                                    }).resume()
                                }
                                
                            }
                    }
            }
    }
    
//    @IBAction func clickOnSEarchBtn(_ sender : UIButton){
//
//        if sender.currentImage == UIImage(named: "search_bg"){
//
//            sender.setImage(UIImage(named: "cross"), for: .normal)
//            self.txtSearchBar.isHidden  = false
//            self.imgTitle.isHidden=true
//            self.hideKeyboardWhenTappedAround()
//
//        }else{
//            sender.setImage(UIImage(named: "search_bg"), for: .normal)
//            self.txtSearchBar.isHidden  = true
//            self.txtSearchBar.text = ""
//            self.imgTitle.isHidden=false
//            self.filteredData = self.arrData
//            self.episodeTable.reloadData()
//        }
//
//    }
    
    func getArray() -> [String] {
        let myarray = defaults.stringArray(forKey: "SavedStringArray") ?? [String]()
        return myarray
    }
    
    func setArray(ary: [String]){
        defaults.set(ary, forKey: "SavedStringArray")
    }
 //=====================================================
    
    @IBAction func ActionFindYourShow(_ sender: Any)
    {
        let findCon = self.storyboard?.instantiateViewController(withIdentifier: "PodcastsTableViewController") as! PodcastsTableViewController
        self.navigationController?.pushViewController(findCon, animated: true)
    }
    
    
    
    
    
  //====================================================================
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //==============================================================
    func alertController(message : String, alert : String)
    {
        let alertController = UIAlertController(title: alert, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(alertAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func startNowPlayingAnimation(_ animate: Bool) {
        
        animate ? nowPlayingImageView.imageView?.startAnimating() : nowPlayingImageView.imageView?.stopAnimating()
    }
    
  
    
    // download episode list
    
    func get_data_from_url(_ link:String)
    {
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "GET"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            (
            data, response, error) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                return
            }
            
            self.extract_json(data!)
        })
        
        task.resume()
        
    }
    
    func extract_json(_ data: Data)
    {
        
        let json: Any?
        do{
            json = try JSONSerialization.jsonObject(with: data, options: [])
        }
        catch{
            return
        }
        
        guard let data_list = json as? NSArray else {
            return
        }
        
        if let shows_list = json as? NSArray
        {
            arrData.removeAll()
            
            for i in 0 ..< data_list.count
            {
                var episodeobj = episode()
                if let shows_obj = shows_list[i] as? NSDictionary
                {
                    
                    let episode_name = shows_obj["episode"] as? String
                    let episode_show = shows_obj["show"] as? String
                    let epside_audio = shows_obj["url"] as? String
                    let episode_date = shows_obj["date"] as? String
                    let paywall = shows_obj["paywall"] as? String
                    let episode_desc = shows_obj["description"] as? String
        
                    episodeobj.show = episode_show!
                    episodeobj.name = episode_name!
                    episodeobj.audio = epside_audio
                    episodeobj.date = episode_date!
                    episodeobj.image = shows_obj["image"] as? String
                    episodeobj.paywall = paywall!
                    episodeobj.description = episode_desc!
                    
                    let dateformate = DateFormatter()
                    dateformate.dateFormat = "MM-dd-yyyy"
                    
                    let e_date = dateformate.date(from: episode_date!)
                    dateformate.dateFormat = "MMMM dd, yyyy"
                    
                    let s_date = dateformate.string(from: e_date!)
                    episodeobj.date = s_date
                    
                    let epdate = dateformate.date(from: s_date)
                    if epdate == nil {
                        
                        episodeobj.episode_date = Date()
                    }
                    else {
                        
                        episodeobj.episode_date = epdate
                    }
                    //
                }
                
//                if arrShow.contains(episodeobj.show) {
//
//                    arrData.append(episodeobj)
//                }
                arrData.append(episodeobj)
                //self.arrData = self.arrData.sorted(by: {$0.episode_date > $1.episode_date })
                self.arrData = self.arrData.sorted(by: {$0.show > $1.show })
                print(arrData)
                let when = DispatchTime.now() + 0.2
                
                DispatchQueue.main.asyncAfter(deadline: when) {
                    
                    self.episodeTable.reloadData()
                }
                
            }
            
            // DispatchQueue.main.async(execute: {self.getDownloadAudio()})
        }
        
    }
    
    struct episode {
        
        var name = ""
        var date = ""
        var audio : String?
        var show = ""
        var description = ""
        var image : String?
        var paywall = ""
        var episode_date : Date!
        var tblname = ""
        var supportStatus = ""
        
        init(name:String,date:String,audio:String?, description:String, image:String?, paywall:String,episode_date:Date,tblname:String, supportStatus:String) {
            
            self.name = name
            self.date = date
            self.audio = audio
            self.description = description
            self.image = image
            self.paywall = paywall
            self.episode_date = episode_date
            self.tblname = tblname
            self.supportStatus = supportStatus
        }
        init() {
            
            self.name = ""
            self.date = ""
            self.audio = ""
            self.show = ""
            self.description = ""
            self.image = ""
            self.paywall = ""
            self.episode_date = Date()
            self.name = ""
            self.supportStatus = ""
           
        }
    }
    
}

//extension  FirstViewController : UITextFieldDelegate{
//
//    @objc func searchWorkersAsPerText(_ textfield:UITextField) {
//        self.filteredData.removeAll()
//        if textfield.text?.count != 0 {
//            for dicData in self.arrData {
//                let isMachingWorker : NSString = (dicData.name) as NSString
//                let range = isMachingWorker.lowercased.range(of: textfield.text!, options: NSString.CompareOptions.caseInsensitive, range: nil,   locale: nil)
//                print(range)
//                if range != nil {
//                    filteredData.append(dicData)
//                }
//            }
//        } else {
//            self.filteredData = self.arrData
//        }
//        self.episodeTable.reloadData()
//    }
//
//
//}


//extension FirstViewController {
//    func hideKeyboardWhenTappedAround() {
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(FirstViewController.dismissKeyboard))
//        tap.cancelsTouchesInView = false
//        view.addGestureRecognizer(tap)
//    }
//
//    @objc func dismissKeyboard() {
//        view.endEditing(true)
//    }
//}
