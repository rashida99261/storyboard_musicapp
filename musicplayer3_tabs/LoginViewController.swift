//
//  LoginViewController.swift
//  musicplayer3_tabs
//
//  Created by Kindlebit on 23/05/18.
//  Copyright © 2018 storyboard. All rights reserved.
//

import UIKit
import AFNetworking


class LoginViewController: UIViewController
{
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passField: UITextField!
    
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var btnBack : UIButton!
    
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(isLoginFrom == "No_Orgn"){
            btnBack.isHidden = true
        }else{
            btnBack.isHidden = false
        }
        
        
        navigationController?.isNavigationBarHidden=true
    }

    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func ActionGoBack(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.view.endEditing(true)
//    }
    
    @IBAction func ActionCheck(_ sender: UIButton) {
    }
    
    @IBAction func ActionLogin(_ sender: UIButton) {
        if emailField?.text != "" && passField.text != ""{
            let params = ["email": emailField.text!,
                          "password": passField.text!]
                as NSDictionary
            self.callApiForResponse(url: Constant.signinUrl, param: params, strCheck: "login")
        }
        else{
            ApiResponse.alert(title: "Alert", message: "Please fill the form.", controller: self)
        }
    }
   
    @IBAction func ActionRegisterC(_ sender: UIButton) {   // change that is Signup
        let regisObj = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(regisObj, animated: true)
        
    }

    @IBAction func ActionVerify(_ sender: Any)
    {
        let verifyCon = self.storyboard?.instantiateViewController(withIdentifier: "verificationViewController") as! verificationViewController
        self.navigationController?.pushViewController(verifyCon, animated: true)
        
    }
}

extension LoginViewController{
    
    
    
    func callApiForResponse(url : String , param : NSDictionary , strCheck : String){
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ApiResponse.onResponsePost(url: url, parms: param) { (dict, error) in
            
            if(error == ""){
                 if(strCheck == "login"){
                    print(dict)
                    let resp = dict["data"] as! NSDictionary
                    let is_verify = resp["is_verify"] as! Bool
                    OperationQueue.main.addOperation {
                        if(is_verify == true){
                            OperationQueue.main.addOperation {
                                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                                let userData = dict["data"] as! NSDictionary
                                let encodedData = NSKeyedArchiver.archivedData(withRootObject: userData)
                                userDef.set(encodedData, forKey: "userData")
                                userDef.set("login", forKey: "session")
                                
                                if(isLoginFrom == "second"){
                                    self.navigationController?.popViewController(animated: true)
                                }
                                else if(isLoginFrom == "podcast"){
                                    let first = self.storyboard?.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
                                    self.navigationController?.pushViewController(first, animated: true)
                                }
                                else{
                                    let first = self.storyboard?.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
                                    self.navigationController?.pushViewController(first, animated: true)
                                }
                            }
                        }else{
                            let first = self.storyboard?.instantiateViewController(withIdentifier: "verificationViewController") as! verificationViewController
                            self.navigationController?.pushViewController(first, animated: true)
                        }
                    }
                }
            }
            else{
                OperationQueue.main.addOperation {
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    if(Constant.statusCode == 401){
                        ApiResponse.alert(title: "Alert", message: "Invalid login credentials. Please try again.", controller: self)
                    }
                    else{
                        ApiResponse.alert(title: "Alert", message: "Something Went Wrong.", controller: self)
                    }
                    
                }
            }
        }
    }
}


extension LoginViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField === emailField){
            emailField.resignFirstResponder()
            passField.becomeFirstResponder()
        }
        textField.resignFirstResponder()
        return true
    }
}
