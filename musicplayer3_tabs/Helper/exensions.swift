//
//  exensions.swift
//  musicplayer3_tabs
//
//  Created by mac on 22/11/18.
//

import Foundation

struct episode {
    
    var name = ""
    var date = ""
    var audio : String?
    var show = ""
    var description = ""
    var image : String?
    var paywall = ""
    var episode_date : Date!
    
    init(name:String,date:String,audio:String?, description:String, image:String?, paywall:String,episode_date:Date) {
        
        self.name = name
        self.date = date
        self.audio = audio
        self.description = description
        self.image = image
        self.paywall = paywall
        self.episode_date = episode_date
    }
    init() {
        
        self.name = ""
        self.date = ""
        self.audio = ""
        self.show = ""
        self.description = ""
        self.image = ""
        self.paywall = ""
        self.episode_date = Date()
    }
}


extension String{
    
    func ChangeDateFormat(Date: String, fromFormat: String, toFormat: String ) -> String  {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        let newdate = dateFormatter.date(from: Date)
        dateFormatter.dateFormat = toFormat
        return dateFormatter.string(from: newdate!)
    }
}
