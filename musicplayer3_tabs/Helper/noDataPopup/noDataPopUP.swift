//
//  noDataPopUP.swift
//  musicplayer3_tabs
//
//  Created by mac on 22/11/18.
//

import UIKit

class noDataPopUP: UIView {

    @IBOutlet weak var contentView : UIView!
    @IBOutlet weak var imgNoData : UIImageView!
    @IBOutlet weak var lblMsg : UILabel!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    func commonInit(){
        Bundle.main.loadNibNamed("noDataPopUP", owner: self, options: nil)
        addSubview(contentView)
    }
}
