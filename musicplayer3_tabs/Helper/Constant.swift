//
//  Constant.swift
//  musicplayer3_tabs
//
//  Created by mac on 20/11/18.
//

import UIKit
class Constant {

    static let baseUrl = "http://13.58.94.220/api/"
    static let img_baseUrl = "http://13.58.94.220"
    static let verifyUrl = "\(Constant.baseUrl)verifications/verifiy_code"
    static let signinUrl = "\(Constant.baseUrl)auth/sign_in"
    //static let FeedUrl = "\(Constant.baseUrl)podcasts/all_podcast"
    static let FeedUrl_Organisation = "\(Constant.baseUrl)podcasts"
    static let Subscribe_Organisation = "\(Constant.baseUrl)organizations"
    static let search = "\(Constant.baseUrl)search"
    
   // static let request_subscription = "\(Constant.baseUrl)request_subscription"
   // static let subscribe = "\(Constant.baseUrl)subscribe"
  //  static let unsubscribe = "\(Constant.baseUrl)unsubscribe"
    
    
    static let Status_Not_200 = "Status code not 200"
    static let Status_Not_201 = "Status code not 201"
    
    static var statusCode = 0
}
