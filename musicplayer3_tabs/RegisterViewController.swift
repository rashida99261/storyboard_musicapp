//
//  RegisterViewController.swift
//  musicplayer3_tabs
//
//  Created by Kindlebit on 23/05/18.
//  Copyright © 2018 storyboard. All rights reserved.
//

import UIKit
import AFNetworking

class RegisterViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var txt_first_name : UITextField?
    @IBOutlet weak var txt_email : UITextField?
    @IBOutlet weak var txt_username : UITextField?
    @IBOutlet weak var txt_password : UITextField?
    @IBOutlet weak var btn_check : UIButton?
    @IBOutlet weak var termsConditions: UILabel!
    @IBOutlet weak var termsButton: UIButton!
    
    @IBOutlet weak var privacyButton: UIButton!
    var unchecked = false
    
    //var ref: DatabaseReference!

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden=true
//        var titleString = NSMutableAttributedString(string: (termsButton.titleLabel?.text)!)
//        titleString.addAttribute(.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 0, length: titleString.length))
//        // using text on button
//        termsButton.setAttributedTitle(titleString, for: .normal)
//        var titleString1 = NSMutableAttributedString(string: (privacyButton.titleLabel?.text)!)
//        titleString1.addAttribute(.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 0, length: titleString1.length))
//        // using text on button
//        privacyButton.setAttributedTitle(titleString1, for: .normal)
        
       //        var attributedString = NSMutableAttributedString(string: "You are agree our terms & conditions and privacy policy.", attributes: nil)
//        var linkRange = NSRange(location: 18, length: 18)
//        // for the word "link" in the string above
//        var linkAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue] as [NSAttributedStringKey : Any]
//        attributedString.setAttributes(linkAttributes, range: linkRange)
//        // Assign attributedText to UILabel
//        termsConditions.attributedText = attributedString
//        termsConditions.isUserInteractionEnabled = true
        //ref = Database.database().reference()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func termsButtonPressed(_ sender: UIButton)
    {
        UIApplication.shared.openURL(NSURL(string: "http://www.trystoryboard.com/privacy")! as URL)
    }
    @IBAction func privacyButtonPressed(_ sender: UIButton)
    {
       UIApplication.shared.openURL(NSURL(string: "http://www.trystoryboard.com/privacy")! as URL)
    }
    @IBAction func ActionGoBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func ActionCheck(_ sender: UIButton) {
        if unchecked{
            btn_check?.setImage(UIImage(named: "check-box"), for: .normal)
            unchecked = false
        }else{
            btn_check?.setImage(UIImage(named: "checked"), for: .normal)
             unchecked = true
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return  emailTest.evaluate(with: testStr)
        
    }
    
    @IBAction func ActionRegister(_ sender: UIButton) {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        //emailTest.evaluate(with: testStr)
        
        let first_name = txt_first_name?.text
        let username = txt_username?.text
        let email = txt_email?.text
        let password = txt_password?.text
        //email,name,password,device_token,os_type,
        //user_id,technique_name,technique_file
        if first_name == ""{
            self.alertController(message: "Please enter first name.", alert: "Alert")
            return
        }
        else if email == ""{
            self.alertController(message: "Please enter email.", alert: "Alert")
            return
        }
        else if emailTest.evaluate(with: email) == false {
            self.alertController(message: "Please enter valid email.", alert: "Alert")
            return
        }
        else  if username == ""{
            self.alertController(message: "Please enter username.", alert: "Alert")
            return
        }
        else if password == ""{
            self.alertController(message: "Please enter password.", alert: "Alert")
            return
        }
        else if(unchecked==false){
            self.alertController(message: "Please accept first Terms & Condition", alert: "Alert")
            return
        }
        else{
            //let ref = Database.database().reference().child("users").queryOrdered(byChild: "username").queryEqual(toValue : username)
            DispatchQueue.main.async {
                self.ApiRegister();
            
//            let ref = Database.database().reference()
//            ref.child("users").queryOrdered(byChild: "email").queryEqual(toValue : email).observe(.value, with:{ (snapshot: DataSnapshot) in
//                if ( snapshot.value is NSNull )
//                {
//                    let post = [
//                        "first_name":  first_name,
//                        "username": username,
//                        "email":   email,
//                        "password":   password
//                    ]
//                    ref.child("users").childByAutoId().setValue(post)
//                    //self.alertController(message: "Successfully register account", alert: "Alert")
//                    self.txt_first_name?.text = ""
//                    self.txt_username?.text = ""
//                    self.txt_email?.text = ""
//                    self.txt_password?.text = ""
//
//                    //let loginObj = self.storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as! SecondViewController
//                    //self.navigationController?.pushViewController(loginObj, animated: true)
//                    self.navigationController?.popViewController(animated: true)
//
//                    return
//                } else {
//                    for snap in snapshot.children {
//                        print((snap as! DataSnapshot).key)
//
//                        let user_snap = snap as! DataSnapshot
//                        let dict = user_snap.value as! [String: String?]
//                        print(dict)
//                        self.alertController(message: "Username already registered with this name", alert: "Alert")
//                       return
//
//                    }
//                }
//            })
            
            }// dispatch que in main thred
            
            
        }
        
    }
    
    func ApiRegister() {
        var userDeviceToken : String = ""
        if(UserDefaults.standard.object(forKey: "user_deviceToken") != nil){
            userDeviceToken = (UserDefaults.standard.value(forKey: "user_deviceToken") as? String)!
        }else{
            userDeviceToken = ""
        }
        
       MBProgressHUD.showAdded(to: self.view, animated: true)
        let URL = "\(appDelegate.common.Obj.base_url)register.php"
        let params = ["first_name": txt_first_name?.text,
                      "username": txt_username?.text,
                      "email": txt_email?.text,
                      "password": txt_password?.text,
                       "device_id": userDeviceToken
                      ]
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFJSONResponseSerializer()
        
        manager.post(URL, parameters: params, constructingBodyWith: { (form) in
            
        }, progress: { (progress) in
            
        }, success: { (operation, responseObject ) -> Void in
            
            let dict = responseObject as! NSDictionary
            print(dict)
            let code =  (dict.object(forKey: "response") as! NSDictionary).value(forKey: "code") as! String
            if code == "200"{
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                UserDefaults.standard.set((dict.object(forKey: "response") as! NSDictionary).value(forKey: "id") as! String, forKey: "user_id")
                UserDefaults.standard.set((dict.object(forKey: "response") as! NSDictionary).value(forKey: "username") as! String, forKey: "username")
                UserDefaults.standard.set((dict.object(forKey: "response") as! NSDictionary).value(forKey: "email") as! String, forKey: "email")
                UserDefaults.standard.set((dict.object(forKey: "response") as! NSDictionary).value(forKey: "first_name") as! String, forKey: "first_name")
                UserDefaults.standard.set("0", forKey: "pushnotification")
                UserDefaults.standard.synchronize()
            
                
                let appDelegat = UIApplication.shared.delegate as! AppDelegate
                let rootController : UITabBarController = appDelegat.window?.rootViewController as! UITabBarController
                let index = rootController.selectedIndex
                let target = (rootController.viewControllers as! NSArray).object(at: index) as! UINavigationController
                target.popToRootViewController(animated: false)
                rootController.selectedIndex = 1
               // let loginObj = self.storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as! SecondViewController
               // self.navigationController?.pushViewController(loginObj, animated: true)
                //self.navigationController?.popViewController(animated: true)
            }else{
                 MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                self.alertController(message: (dict.object(forKey: "response") as! NSDictionary).value(forKey: "msg") as! String, alert: "Alert")
            }
            
        }) { (operation, error) -> Void in
            print(error)
             MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            self.alertController(message: "\(error)", alert: "Alert")
            
        }
    }
    
    func alertController(message : String, alert : String)
    {
        let alertController = UIAlertController(title: alert, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(alertAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == txt_first_name)
        {
            var limitLength = 30
            guard let text = txt_first_name?.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= limitLength
        }
        else if (textField == txt_email)
        {
            var limitLength = 50
            guard let text = txt_email?.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= limitLength
        }
        else if (textField == txt_username)
        {
            var limitLength = 20
            guard let text = txt_username?.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= limitLength
        }
        else if (textField == txt_password)
        {
            var limitLength = 20
            guard let text = txt_password?.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= limitLength
        }
        else
        {
            return true
        }
    }
    
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
