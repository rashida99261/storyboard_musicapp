//
//  DetailsViewController.swift
//  musicplayer3_tabs
//
//  Created by Mac Mini 102 on 7/27/18.
//  Copyright © 2018 storyboard. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.barTintColor = UIColor.black
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.isTranslucent = false
        self.title = "Subscription Details"
        // Do any additional setup after loading the view.
    }

    @IBAction func privacyButtonPressed(_ sender: UIButton)
    {
        UIApplication.shared.openURL(NSURL(string: "http://www.trystoryboard.com/privacy")! as URL)
    }
    @IBAction func termsButtonPressed(_ sender: UIButton)
    {
        UIApplication.shared.openURL(NSURL(string: "http://www.trystoryboard.com/privacy")! as URL)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
