
import UIKit
import AFNetworking

class downloadCell : UITableViewCell
{
   @IBOutlet weak var lbl_title: UILabel!
   @IBOutlet weak var lbl_size: UILabel!
}
class downloadManageViewController: UIViewController {
    
    var thedownloads : NSMutableArray = NSMutableArray()
    var theurls = [URL]()
    var arrData = [episode]()
    @IBOutlet weak var nowPlayingImageView: UIButton!
    @IBOutlet weak var downloadsTable: UITableView!
    @IBOutlet weak var viewNoData : UIView!
    @IBOutlet weak var btnClearAll : UIButton!
    @IBOutlet weak var contnView : noDataPopUP!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        nowPlayingImageView.imageView?.animationImages = AnimationFrames.createFrames()
        nowPlayingImageView.imageView?.animationDuration = 1.0
        downloadsTable.layoutMargins = UIEdgeInsets.zero
        downloadsTable.separatorInset = UIEdgeInsets.zero
        // Do any additional setup after loading the view.

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func startNowPlayingAnimation(_ animate: Bool) {
        
        animate ? nowPlayingImageView.imageView?.startAnimating() : nowPlayingImageView.imageView?.stopAnimating()
    }
    @IBAction func click_Wave(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EpisodeViewController") as! EpisodeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        let loginSes = userDef.value(forKey: "session")
        if(loginSes == nil){
            self.downloadsTable.isHidden = true
            self.btnClearAll.isHidden = true
            self.viewNoData.isHidden = false
            self.contnView.lblMsg.text = "Please login."
        }else{
            self.viewNoData.isHidden = true
            let outData = userDef.data(forKey: "userData")
            let dict = NSKeyedUnarchiver.unarchiveObject(with: outData!)as! NSDictionary
            let organisatin_id = dict.value(forKey: "organization_id") as? Int
            if(organisatin_id == nil){
                self.downloadsTable.isHidden = true
                self.btnClearAll.isHidden = true
                self.viewNoData.isHidden = false
                self.contnView.lblMsg.text = "No organisation available to dowload podcast."
            }else{
                MBProgressHUD.showAdded(to: self.view, animated: true)
                get_data_from_url(Constant.FeedUrl_Organisation)
            }
        }
        
        downloadsTable.tableFooterView = UIView()
        if (audioPlayer != nil) {
            if  audioPlayer.isPlaying {
                nowPlayingImageView.isHidden = false
                startNowPlayingAnimation(true)
            }else{
                nowPlayingImageView.isHidden = true
                startNowPlayingAnimation(false)
            }
        }
        else {
            nowPlayingImageView.isHidden = true
            startNowPlayingAnimation(false)
        }
    }
    
    @IBAction func deleteDirectoryButton(_ sender: UIButton) {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        do {
            let fileURLs = try FileManager.default.contentsOfDirectory(at: documentsUrl,
                                                                       includingPropertiesForKeys: nil,
                                                                       options: [.skipsHiddenFiles, .skipsSubdirectoryDescendants])
            for fileURL in fileURLs {
                if fileURL.pathExtension == "mp3" {
                    try FileManager.default.removeItem(at: fileURL)
                }
                if fileURL.pathExtension == "m4a" {
                    try FileManager.default.removeItem(at: fileURL)
                }
                if fileURL.pathExtension == "wav" {
                    try FileManager.default.removeItem(at: fileURL)
                }
            }
            
            

            
            thedownloads.removeAllObjects()
            downloadsTable.reloadData()
            self.downloadsTable.isHidden = true
            self.viewNoData.isHidden = false
            self.btnClearAll.isHidden = true
            self.contnView.lblMsg.text = "No downloads available."
            
        } catch  { print(error) }
    }
    
    func get_data_from_url(_ link:String)
    {
       // MBProgressHUD.showAdded(to: self.view, animated: true)
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        
        let manager = AFURLSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        let req: NSMutableURLRequest? = AFJSONRequestSerializer().request(withMethod: "GET", urlString: link, parameters: nil, error: nil)
        
        let uid = userDef.value(forKey: "uid") as! String
        let access_token = userDef.value(forKey: "access-token") as! String
        let client = userDef.value(forKey: "client") as! String
        
        req?.setValue("application/json", forHTTPHeaderField: "content-type")
        req?.setValue("\(uid)", forHTTPHeaderField: "uid")
        req?.setValue("\(access_token)", forHTTPHeaderField: "access-token")
        req?.setValue("\(client)", forHTTPHeaderField: "client")
        
        
        if let aReq = req {
            (manager.dataTask(with: aReq as URLRequest, completionHandler: { response, responseObject, error in
                if error == nil {
                    
                    OperationQueue.main.addOperation {
                        if let shows_list = responseObject as? NSDictionary
                        {
                            let data =  shows_list.object(forKey: "data") as! [NSDictionary]
                            self.arrData.removeAll()
                            for i in 0 ..< data.count
                            {
                                var episodeobj = episode()
                                let shows_obj = data[i]
                                if let episode_show = shows_obj["show_id"] as? String{
                                    episodeobj.show = episode_show
                                }
                                if let episode_name = shows_obj["name"] as? String{
                                    episodeobj.name = episode_name
                                }
                                let audioDict = shows_obj["audio"] as! NSDictionary
                                if let epside_audio = audioDict["url"] as? String{
                                    episodeobj.audio = epside_audio
                                }
                                if let episode_date = shows_obj["date"] as? String{
                                    episodeobj.date = episode_date
                                }
                                self.arrData.append(episodeobj)
                                self.arrData = self.arrData.sorted { $0.name < $1.name }
                            }
                            DispatchQueue.main.async(execute: {self.getDownloadAudio()})
                        }
                    }
                    
                    }
                })).resume()
        }
        
        
       
    }
    
    func getDownloadAudio() {
        
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        do {
            thedownloads.removeAllObjects()
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: [])
            theurls = directoryContents
            
            // if you want to filter the directory contents you can do like this:
            let mp3Files = directoryContents.filter{ $0.pathExtension == "mp3" || $0.pathExtension == "m4a" ||  $0.pathExtension == "wav"}
            print("mp3 urls:",mp3Files)
            
            let mp3FileNames = mp3Files.map{ $0.deletingPathExtension().lastPathComponent }
            print("mp3 list:", mp3FileNames)
            var index = 0
            for mp3file in mp3FileNames {
                for obj in arrData {
                    let url = obj.audio?.removingPercentEncoding
                    if (url?.contains(mp3file))! {
                        let filePath = directoryContents[index].path
                        let attributes = try FileManager.default.attributesOfItem(atPath: filePath)
                        let folderSize = attributes[FileAttributeKey.size] as? Int64 ?? 0
                        let fileSizeStr = ByteCountFormatter.string(fromByteCount: folderSize, countStyle: ByteCountFormatter.CountStyle.file)
                        print(fileSizeStr)
                        
                        index = index + 1
                        let dict = NSMutableDictionary()
                        dict.setValue(obj.name, forKey: "show")
                        dict.setValue(obj.date, forKey: "date")
                        dict.setValue(fileSizeStr, forKey: "size")
                        thedownloads.add(dict)
                        break
                    }
                }
            }
            
            if(thedownloads.count == 0){
                self.downloadsTable.isHidden = true
                self.btnClearAll.isHidden = true
                self.viewNoData.isHidden = false
                self.contnView.lblMsg.text = "No downloads available."
            }else{
                self.viewNoData.isHidden = true
                self.downloadsTable.isHidden = false
                self.btnClearAll.isHidden = false
                downloadsTable.reloadData()
            }
        } catch {
            print(error.localizedDescription)
        }
        print(thedownloads)
    }
}

extension  downloadManageViewController : UITableViewDelegate,UITableViewDataSource{
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return thedownloads.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = downloadCell()
        cell.layoutMargins = UIEdgeInsets.zero
        cell =  tableView.dequeueReusableCell(withIdentifier: "showcell", for: indexPath) as! downloadCell
        cell.lbl_title.text = ((thedownloads.object(at: indexPath.row) as! NSDictionary).value(forKey: "show") as! String)
        
        let datePrevious = ((thedownloads.object(at: indexPath.row) as! NSDictionary).value(forKey: "date") as! String)
        let DateFormateChange = datePrevious.ChangeDateFormat(Date: datePrevious, fromFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSXXX", toFormat: "MM-dd-yyyy")
        cell.lbl_size.text = DateFormateChange
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let url = thedownloads[indexPath.row]
        print(url)
        //kndl commented below code
        //        do {
        //            try FileManager.default.removeItem(at: theurls[indexPath.row])
        //            // self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        //            // self.tableView.reloadData()
        //        } catch let error {
        //            print(error)
        //        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            thedownloads.removeObject(at: indexPath.row)
            //thedownloads.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            
            
            do {
                try FileManager.default.removeItem(at: theurls[indexPath.row])
                
                if(thedownloads.count == 0){
                    self.downloadsTable.isHidden = true
                    self.viewNoData.isHidden = false
                    self.btnClearAll.isHidden = true
                    self.contnView.lblMsg.text = "No downloads available."
                }else{
                    self.viewNoData.isHidden = true
                    self.downloadsTable.isHidden = false
                    self.btnClearAll.isHidden = false
                    downloadsTable.reloadData()
                }
                
            } catch let error {
                print(error)
            }
            // end if
        }
    }
}
