//
//  PodcastsTableViewController.swift
//  musicplayer3_tabs
//
//  Created by JP on 1/20/18.
//  Copyright © 2018 storyboard. All rights reserved.
//
import UIKit
import AFNetworking
class podcastCell : UITableViewCell
{
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var subscribeImageView: UIImageView!
}

var isLoginFrom = ""
var isNAvigateFrom = ""

class PodcastsTableViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    var TableData:Array< String > = Array < String >()
     @IBOutlet var tblPodcast: UITableView!
    @IBOutlet weak var nowPlayingImageView: UIButton!
    var mydictArr : NSMutableArray = NSMutableArray()
    let defaults = UserDefaults.standard
    var show_image = ""
    var myarray : NSMutableArray = NSMutableArray()
    var myArray : NSArray? = NSArray()
    var subscribedArray : NSMutableArray = NSMutableArray()
    @IBOutlet weak var switchPhus : UISwitch!
    
    
    var arrData : [NSDictionary] = []
    @IBOutlet weak var viewNoData : UIView!
    @IBOutlet weak var contnView : noDataPopUP!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden=true
        //switchPhus.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        
        //        UserDefaults.standard.set((dict.object(forKey: "response") as! NSDictionary).value(forKey: "pushnotification") as! String, forKey: "pushnotification")
        //        UserDefaults.standard.synchronize()
        
        
        nowPlayingImageView.imageView?.animationImages = AnimationFrames.createFrames()
        nowPlayingImageView.imageView?.animationDuration = 1.0
        navigationController?.isNavigationBarHidden=false
        navigationController?.navigationBar.barTintColor = UIColor.black
        //navigationController?.navigationBar.backgroundColor = UIColor.black
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.isTranslucent = false
      //  tblPodcast.layoutMargins = UIEdgeInsets.zero
       // tblPodcast.separatorInset = UIEdgeInsets.zero
        
        tblPodcast.tableFooterView = UIView()
        
        
       
    }
    
    

    
    
    
    
    
    
    @IBAction func ActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
   
  
    override func viewWillAppear(_ animated: Bool) {
        self.tblPodcast.isHidden = true
        self.viewNoData.isHidden = true
        
        
        let loginSes = userDef.value(forKey: "session")
        if(loginSes == nil){
            self.ApiGetUserSub()
        }else{
            self.ApiGetUserSubWithLogin()
        }
        
        navigationController?.isNavigationBarHidden=true
        self.tblPodcast.reloadData()
        if (audioPlayer != nil) {
            if  audioPlayer.isPlaying {
                nowPlayingImageView.isHidden = false
                startNowPlayingAnimation(true)
            }else{
                nowPlayingImageView.isHidden = true
                startNowPlayingAnimation(false)
            }
        }
        else {
            nowPlayingImageView.isHidden = true
            startNowPlayingAnimation(false)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func ApiGetUserSub() {
        let URL = "\(Constant.search)"
        self.arrData.removeAll()
        let manager = AFHTTPSessionManager()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.get(URL, parameters: nil, progress: { (progress) in
            print(progress)
            
        }, success: { (operation, responseObject ) -> Void in
            if let dict = responseObject as? NSDictionary{
                print(dict)
                let success = dict["success"] as! String
                if(success == "false"){
                    let msg = dict["message"] as! String
                    ApiResponse.alert(title: "Alert", message: msg, controller: self)
                }else{
                    self.arrData =  dict.object(forKey: "data") as! [NSDictionary]
                    if(self.arrData.count == 0){
                        self.tblPodcast.isHidden = true
                        self.viewNoData.isHidden = false
                        self.contnView.lblMsg.text = "No podcast available."
                    }else{
                        self.viewNoData.isHidden = true
                        self.tblPodcast.isHidden = false
                        self.tblPodcast.reloadData()
                    }
                }
            }
            if let dict = responseObject as? [NSDictionary]{
                print(dict)
                    self.arrData = dict
                    if(self.arrData.count == 0){
                        self.tblPodcast.isHidden = true
                        self.viewNoData.isHidden = false
                        self.contnView.lblMsg.text = "No podcast available."
                    }else{
                        self.viewNoData.isHidden = true
                        self.tblPodcast.isHidden = false
                        self.tblPodcast.reloadData()
                    }
            }
        }) { (operation, error) -> Void in
            ApiResponse.alert(title: "Alert", message: "Somthing went wrong!", controller: self)
        }
    }
    
    
    func ApiGetUserSubWithLogin() {
        
        self.arrData.removeAll()
        var error: Error?
        let manager = AFURLSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        let req: NSMutableURLRequest? = AFJSONRequestSerializer().request(withMethod: "GET", urlString: Constant.search, parameters: nil, error: nil)
        
        let uid = userDef.value(forKey: "uid") as! String
        let access_token = userDef.value(forKey: "access-token") as! String
        let client = userDef.value(forKey: "client") as! String
        
        req?.setValue("application/json", forHTTPHeaderField: "content-type")
        req?.setValue("\(uid)", forHTTPHeaderField: "uid")
        req?.setValue("\(access_token)", forHTTPHeaderField: "access-token")
        req?.setValue("\(client)", forHTTPHeaderField: "client")
        
        print("\(uid)-----\(access_token)----\(client)")
        
        if let aReq = req {
            (manager.dataTask(with: aReq as URLRequest, completionHandler: { response, responseObject, error in
                
                if error == nil {
                    if let anObject = responseObject {
                        print(anObject)
                        
                        if let dict = anObject as? [NSDictionary]{
                            print(dict)
                            self.arrData = dict
                            
                           // self.TableDataV.sort(by: { $0.episode_date > $1.episode_date })
                            
                            if(self.arrData.count == 0){
                                self.tblPodcast.isHidden = true
                                self.viewNoData.isHidden = false
                                self.contnView.lblMsg.text = "No podcast available."
                            }else{
                                self.viewNoData.isHidden = true
                                self.tblPodcast.isHidden = false
                                self.tblPodcast.reloadData()
                            }
                        }
                        
                    }
                } else {
                    if let anError = error, let anObject = responseObject {
                        print("Error: \(anError), \(response), \(anObject)")
                    }
                }
            })).resume()
        }
    }
    
    func startNowPlayingAnimation(_ animate: Bool) {
        
        animate ? nowPlayingImageView.imageView?.startAnimating() : nowPlayingImageView.imageView?.stopAnimating()
    }
    
    // MARK: - Table view data source
    
     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.arrData.count
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 121;//Choose your custom row height
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = podcastCell()
        cell.layoutMargins = UIEdgeInsets.zero
        cell = tblPodcast.dequeueReusableCell(withIdentifier: "showcell", for: indexPath) as! podcastCell
        
        let loginSes = userDef.value(forKey: "session")
        if(loginSes == nil){
            
            cell.lblStatus.isHidden = true
            let dict = self.arrData[indexPath.row]
            let imgDict = dict["image"] as! NSDictionary
            let url = imgDict["url"] as! String
            let url_img = "\(Constant.img_baseUrl)\(url)"
            let imageURL = URL(string: url_img)
            if  imageURL != nil {
                cell.iconImageView.sd_setImage(with: imageURL, placeholderImage: nil, options: .continueInBackground, completed: nil)
            } else {
                cell.iconImageView.image = UIImage(named: "placeholder")
            }
            
            cell.nameLabel.text = (dict["organization_name"] as! String)
            cell.nameLabel.numberOfLines = 2
            
        }else{
            
            
            
            let dict = self.arrData[indexPath.row]
            print(dict)
            
            if let arrReqUser = dict["request_users"] as? [NSDictionary]{
                print(arrReqUser)
                if(arrReqUser.count>0){
                    let dictt = arrReqUser[0]
                   let usrId = dictt["user_id"] as! Int
                    let outData = userDef.data(forKey: "userData")
                    let loginDict = NSKeyedUnarchiver.unarchiveObject(with: outData!)as! NSDictionary
                    if(usrId == (loginDict["id"] as! Int)){
                        cell.lblStatus.isHidden = false
                        cell.lblStatus.text = dictt["status"] as? String
                    }
                }else{
                    cell.lblStatus.isHidden = true
                }
            }
            let imgDict = dict["image"] as! NSDictionary
            let url = imgDict["url"] as! String
            let url_img = "\(Constant.img_baseUrl)\(url)"
            let imageURL = URL(string: url_img)
            if  imageURL != nil {
                cell.iconImageView.sd_setImage(with: imageURL, placeholderImage: nil, options: .continueInBackground, completed: nil)
            } else {
                cell.iconImageView.image = UIImage(named: "placeholder")
            }
            
            cell.nameLabel.text = (dict["organization_name"] as! String)
            cell.nameLabel.numberOfLines = 2
        }
      
        
        return cell
    }
    
    @objc func tapaccessoryButton(sender:UITapGestureRecognizer) {
        
        let tag = sender.view?.tag
        let indexpath = IndexPath(row: tag!, section: 0)
        if indexpath != nil {
            self.tableView(tblPodcast, accessoryButtonTappedForRowWith: indexpath)
        }
    }
    
     func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        // test to see if i can store row name in the defaults array
        let showName = (mydictArr.object(at: indexPath.row) as! NSDictionary).value(forKey: "show_name") as! String
        let showThumbnail = (mydictArr.object(at: indexPath.row) as! NSDictionary).value(forKey: "image") as! String
        let index = indexPath.row
        let mydict : NSMutableDictionary = NSMutableDictionary()
        
        mydict.setValue(showName, forKey: "show_name")
        mydict.setValue(showThumbnail, forKey: "image")
//        mydict.setValue("subscribed", forKey: "status")
//        mydict.setValue("podcast_id", forKey: "podcast_id")
//        mydict.setValue("episode", forKey: "episode")
//        mydict.setValue("url", forKey: "url")
//        mydict.setValue("description", forKey: "description")
//        mydict.setValue("paywall", forKey: "paywall")
//        mydict.setValue("date", forKey: "date")
        
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let loginSes = userDef.value(forKey: "session")
        if(loginSes == nil){
            let alert = UIAlertController(title: "Alert", message:  "Please confirm your access to this organization with your Storyboard code." , preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    isLoginFrom = "podcast"
                    self.navigationController?.pushViewController(login, animated: true)
                case .cancel:
                    print("cancel")
                case .destructive:
                    print("jfhjfh")
                }}))
            self.present(alert, animated: true, completion: nil)
            return
        }
        else{
            let dict = self.arrData[indexPath.row]
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as! SecondViewController
            
            let arrReqUser = dict["request_users"] as! [NSDictionary]
            if(arrReqUser.count>0){
                let dictt = arrReqUser[0]
                let status = dictt["status"] as! String
                controller.statusSub = status
            }
            controller.ordanisation_data = dict
            isNAvigateFrom = "podcastTable"
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
   
    @IBAction func Click_wave(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EpisodeViewController") as! EpisodeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

