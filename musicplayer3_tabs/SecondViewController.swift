

import UIKit
import AVFoundation
import SDWebImage
import AFNetworking
import StoreKit
import SwiftyStoreKit
import DropDown
var audiotest = ""
var organisationId = 0

var audioPlayer:AVAudioPlayer!

class episodeClassCell: UITableViewCell{
    @IBOutlet weak var lbl_episodetitle: UILabel!
    @IBOutlet weak var lbl_episodeDate: UILabel!
}

class SecondViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    let defaults = UserDefaults.standard
    var myarray : NSMutableArray = NSMutableArray()
    var arraySuppored : NSMutableArray = NSMutableArray()
    var topTab : Bool = true
    let dropdown = DropDown()
    @IBOutlet weak var btnLogin : UIButton?
    @IBOutlet weak var tblOrganisation : UITableView!
    
    var TableDataV : [episode] = [episode]()
    var showNameVariable = ""
    var showImageUrl = ""
    var showpodcast_id = ""
    var showepisode = ""
    var showurl = ""
    var showdescription = ""
    var showimage = ""
    var showpaywall = ""
    var showdate = ""
    var showCat = ""
    var checkSubscription = ""
    var checkSupport = ""
    var productIdentifier = "storyboard_subscription" //Get it from iTunes connect
    var productId = "storyboard_subscription"
    var productsRequest = SKProductsRequest()
    var iapProducts = [SKProduct]()
    var subscriptionPurchaseMade = UserDefaults.standard.bool(forKey: "subscriptionPurchaseMade")
    let appBundleId = "com.storyboardapp"
    var isPremium = String()
    var activity_indicator = UIActivityIndicatorView()
    @IBOutlet weak var viewImg: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var btn_Subscribr: UIButton!
    
    @IBOutlet weak var viewLogin : UIView!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    
    @IBOutlet weak var nowPlayingImageView: UIButton!
    
    @IBOutlet weak var viewNoData : UIView!
    @IBOutlet weak var lblMsg : UILabel!
    
    var statusSub = ""
    
    
    
    
    @IBOutlet weak var contnView : noDataPopUP!
    
    
    var ordanisation_data : NSDictionary = [:]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        topTab = true
        
        tblOrganisation.layoutMargins = UIEdgeInsets.zero
        tblOrganisation.separatorInset = UIEdgeInsets.zero
        self.navigationController?.isNavigationBarHidden=true
        
        nowPlayingImageView.imageView?.animationImages = AnimationFrames.createFrames()
        nowPlayingImageView.imageView?.animationDuration = 1.0
        
        
        activity_indicator.frame = CGRect(x: 50, y: 50, width: 20, height: 20)
        activity_indicator.isHidden = true
        activity_indicator.center = self.view.center
        activity_indicator.color = UIColor.black
        self.view.addSubview(activity_indicator)
        tblOrganisation.layoutMargins = UIEdgeInsets.zero
        tblOrganisation.separatorInset = UIEdgeInsets.zero
        
        
    }
    
    @IBAction func ClickOnBackBtn(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func click_Wave(_ sender: UIButton){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EpisodeViewController") as! EpisodeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    
    //Subscribe and unsub
    @IBAction func clickOnSubscribeBtn(_ sender : UIButton){
        
    }
    
    func startNowPlayingAnimation(_ animate: Bool) {
        
        animate ? nowPlayingImageView.imageView?.startAnimating() : nowPlayingImageView.imageView?.stopAnimating()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.btnBack.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        tblOrganisation.tableFooterView = UIView()
        self.btnLogin?.layer.cornerRadius = 20
        
        let loginSes = userDef.value(forKey: "session")
        if(loginSes == nil){
            
            self.viewLogin.isHidden=false
            self.tblOrganisation.isHidden=true
            self.viewNoData.isHidden = true
            self.myarray.removeAllObjects()
            self.btnProfile.isHidden=true
            
        }
        else{
            
            self.tblOrganisation.isHidden=false
            self.btnProfile.isHidden=false
             self.viewLogin.isHidden=true
            let outData = userDef.data(forKey: "userData")
            let dict = NSKeyedUnarchiver.unarchiveObject(with: outData!)as! NSDictionary
            print(dict)
            let organisatin_id = dict.value(forKey: "organization_id") as? Int
            if(organisatin_id == nil){
                    self.viewNoData.isHidden = true
                    self.tblOrganisation.isHidden = true
                    self.contnView.lblMsg.text = "You have no subscribed organisation."
            }else{
                    btn_Subscribr.setTitle("Subscribed", for: .normal)
                    self.tblOrganisation.isHidden = false
                    self.viewNoData.isHidden = true
                    self.apiForOrganisationFeed()
                    self.ApiGetUserSubscriber()
            }
                //self.ApiGetUserSubscriber()
        }
        
        btn_Subscribr.backgroundColor = UIColor(red: 21/255.0, green: 134/255.0, blue: 50/255.0, alpha: 1)
        btn_Subscribr.layer.cornerRadius = 17
        
        if (audioPlayer != nil) {
            if  audioPlayer.isPlaying {
                
                nowPlayingImageView.isHidden = false
                startNowPlayingAnimation(true)
                
            }else{
                nowPlayingImageView.isHidden = true
                startNowPlayingAnimation(false)
                
            }
            
        }
        else {
            nowPlayingImageView.isHidden = true
            startNowPlayingAnimation(false)
        }
    }
    
    func get_data_from_url(_ link:String)
    {
        //kndl remove loader
        //MBProgressHUD.showAdded(to: self.view, animated: true)
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "GET"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            (
            data, response, error) in
            
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                
                return
            }
            
            self.extract_json(data!)
        })
        
        task.resume()
        
    }
    
    
    func extract_json(_ data: Data)
    {
        //MBProgressHUD.showAdded(to: self.view, animated: true)
        //        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true);
        //        spinnerActivity?.isUserInteractionEnabled = false;
        let json: Any?
        do{
            json = try JSONSerialization.jsonObject(with: data, options: [])
        }
        catch{
            return
        }
        
        guard let data_list = json as? NSArray else {
            return
        }
        
        if let shows_list = json as? NSArray
        {
            TableDataV.removeAll()
            
            for i in 0 ..< data_list.count
            {
                if let shows_obj = shows_list[i] as? NSDictionary
                {
                    print("test" + showNameVariable)
                    if (shows_obj["show"] as? String == "\(showNameVariable)") {
                        
                        let episode_name = shows_obj["episode"] as? String
                        let episode_date = shows_obj["date"] as? String

                        let epside_audio = shows_obj["url"] as? String
                        let episode_description = shows_obj["description"] as? String
                        let episode_image = shows_obj["image"] as? String
                        let episode_paywall = shows_obj["paywall"] as? String
                        
                        
                        let dateformate = DateFormatter()
                        dateformate.dateFormat = "MM-dd-yyyy"
                        let e_date = dateformate.date(from: episode_date!)
                        
                        dateformate.dateFormat = "MMMM dd, yyyy"
                        
                        let s_date = dateformate.string(from: e_date!)
                        let finaldate = s_date
                        let epdate = dateformate.date(from: s_date)
                        
                        print(s_date)
                        
                        TableDataV.append(episode(name: episode_name!, date: s_date, audio: epside_audio, description: episode_description!, image: episode_image!, paywall: episode_paywall!,episode_date: e_date!))
                        
                        // sets the image for the table
                        imgView.sd_setImage(with: URL(string: episode_image!), placeholderImage: UIImage(named: "placeholder.png"))
                        
                        
                    }
                    else
                    {
                        print("no matches")
                    }
                }
                
            }
            self.TableDataV.sort(by: { $0.episode_date > $1.episode_date })
            self.TableDataV.sort(by: { $0.name > $1.name })
            
        }
        
        
        DispatchQueue.main.async {
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            self.do_table_refresh()
            
        }
        //DispatchQueue.main.async(execute: {self.do_table_refresh()})
        
    }
    
    
    func do_table_refresh(){
        self.tblOrganisation.reloadData()
    }
    
    
    func ApiGetUserSubscriber() {
        var error: Error?
        let manager = AFURLSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        let req: NSMutableURLRequest? = AFJSONRequestSerializer().request(withMethod: "GET", urlString: Constant.Subscribe_Organisation, parameters: nil, error: nil)
        
        let uid = userDef.value(forKey: "uid") as! String
        let access_token = userDef.value(forKey: "access-token") as! String
        let client = userDef.value(forKey: "client") as! String
        
        req?.setValue("application/json", forHTTPHeaderField: "content-type")
        req?.setValue("\(uid)", forHTTPHeaderField: "uid")
        req?.setValue("\(access_token)", forHTTPHeaderField: "access-token")
        req?.setValue("\(client)", forHTTPHeaderField: "client")
        
        print("\(uid)-----\(access_token)----\(client)")
        
        if let aReq = req {
            (manager.dataTask(with: aReq as URLRequest, completionHandler: { response, responseObject, error in
                
                if error == nil {
                    if let anObject = responseObject {
                        print(anObject)
                        let dict = anObject as! NSDictionary
                        print(dict)
                        if let success = dict["success"] as? String{
                            if(success == "false"){
                                let msg = dict["message"] as! String
                                ApiResponse.alert(title: "Alert", message: msg, controller: self)
                            }else{
                                
                                let data =  dict.object(forKey: "data") as! NSDictionary
                                print(data)
                                
                                let imgDict = data["image"] as! NSDictionary
                                let urlll = imgDict["url"] as! String
                                let url_img = "\(Constant.img_baseUrl)\(urlll)"
                                let imageURL = URL(string: url_img)
                                if  imageURL != nil {
                                    self.imgView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"), options: .continueInBackground, completed: nil)
                                } else {
                                    self.imgView.image = UIImage(named: "placeholder")
                                }
                                
                                self.lbl_name.text = data["name"] as? String
                            }
                        }else{
                            
                            let msg = dict["message"] as! String
                            ApiResponse.alert(title: "Alert", message: msg, controller: self)
                        }
                        
                    }
                } else {
                    if let anError = error, let anObject = responseObject {
                        print("Error: \(anError), \(response), \(anObject)")
                    }
                }
            })).resume()
        }
        
    }
    
    
    func apiForOrganisationFeed(){
        var error: Error?
        let manager = AFURLSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        let req: NSMutableURLRequest? = AFJSONRequestSerializer().request(withMethod: "GET", urlString: Constant.FeedUrl_Organisation, parameters: nil, error: nil)
        
        let uid = userDef.value(forKey: "uid") as! String
        let access_token = userDef.value(forKey: "access-token") as! String
        let client = userDef.value(forKey: "client") as! String
        
        req?.setValue("application/json", forHTTPHeaderField: "content-type")
        req?.setValue("\(uid)", forHTTPHeaderField: "uid")
        req?.setValue("\(access_token)", forHTTPHeaderField: "access-token")
        req?.setValue("\(client)", forHTTPHeaderField: "client")
        
        print("\(uid)-----\(access_token)----\(client)")
        
        if let aReq = req {
            (manager.dataTask(with: aReq as URLRequest, completionHandler: { response, responseObject, error in
                print("Reply JSON: \(responseObject)")
                if error == nil {
                    
                    if let dict = responseObject as? NSArray{
                    }
                    if  let dict = responseObject as?  NSDictionary{
                        print(dict)
                        let success = dict["success"] as! String
                        if(success == "false"){
                            let msg = dict["message"] as! String
                            ApiResponse.alert(title: "Alert", message: msg, controller: self)
                            
                        }else{
                            let data =  dict.object(forKey: "data") as! [NSDictionary]
                            print(data)
                            self.TableDataV.removeAll()
                            print(self.TableDataV)
                            
                            if(data.count == 0){
                                self.tblOrganisation.isHidden = true
                                self.viewNoData.isHidden = false
                                self.lblMsg.text = "No podcast detail to show."
                            }else{
                                for i in 0 ..< data.count
                                {
                                    DispatchQueue.main.async()
                                        {
                                            let dict = data[i]
                                            print(dict)
                                            var episodeobj = episode()
                                            if let episode_name = dict["name"] as? String {
                                                episodeobj.name = episode_name
                                            }
                                            let audioDict = dict["audio"] as! NSDictionary
                                            if let epside_audio = audioDict["url"] as? String{
                                                episodeobj.audio = epside_audio
                                            }
                                            if let episode_date = dict["date"] as? String{
                                                episodeobj.date = episode_date
                                            }
                                            if let episode_show = dict["show_id"] as? String{
                                                episodeobj.show = episode_show
                                            }
                                            
                                            if let episode_desc = dict["description"]  as? String {
                                                episodeobj.description = episode_desc
                                            }
                                            
                                            let img = dict["image"] as! NSDictionary
                                            //  let thumb = img["thumb"] as! NSDictionary
                                            if let episode_image = img["url"]  as? String {
                                                episodeobj.image = episode_image
                                            }
                                            self.TableDataV.append(episodeobj)
                                            self.tblOrganisation.reloadData()
                                    }
                                }
                            }
                            self.tblOrganisation.reloadData()
                        }
                    }
                } else {
                    if let anError = error, let anObject = responseObject {
                        print("Error: \(anError), \(response), \(anObject)")
                    }
                }
            })).resume()
        }
        
    }
    
    
    @IBAction func clickOnProfileBtn(_ sender : UIButton){
        let loginCon = storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(loginCon, animated: true)
    }
    
    @IBAction func ActionLoginC(_ sender: Any) {
        let loginCon = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        isLoginFrom = "second"
        self.navigationController?.pushViewController(loginCon, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TableDataV.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! episodeClassCell
        let epi = TableDataV[indexPath.row]
        cell.lbl_episodetitle.text = epi.name
          let DateFormateChange = epi.date.ChangeDateFormat(Date:epi.date, fromFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSXXX", toFormat: "MM-dd-yyyy")
        
        print(DateFormateChange)
        cell.lbl_episodeDate.text = DateFormateChange
       
        var url = ""
        if (epi.paywall == "no") {
            cell.backgroundColor = UIColor(red:0.89, green:0.95, blue:0.99, alpha:1.0)
        } else {
            cell.backgroundColor = UIColor(red:1.00, green:0.93, blue:0.70, alpha:1.0)
        }
        
        if epi.audio != nil {
            
            url = epi.audio!
            
            if let audioUrl = URL(string: url) {
                
                // then lets create your document folder url
                let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                
                // lets create your destination file url
                let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
                
                print(destinationUrl)
                
                // to check if it exists before downloading it
                if FileManager.default.fileExists(atPath: destinationUrl.path) {
                    print("The file already exists at path")
                    cell.accessoryType = .checkmark
                    cell.accessoryView = nil
                }
                else{
                    
                    // this is the code I am testing
                    let downloadicon = UIImage(named: "download.png")
                    cell.accessoryType = .detailDisclosureButton
                    cell.accessoryView = UIImageView(image: downloadicon)
                }
            } // end audio if
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 90.0;//Choose your custom row height
    }
    @objc func tapaccessoryButtonSupport(sender:UITapGestureRecognizer) {
        let tag = sender.view?.tag
        let indexpath = IndexPath(row: tag!, section: 0)
        //self.tableView(subscribeTable, accessoryButtonTappedForRowWith: indexpath)
        self.supportButtonTapped(indexPath: indexpath )
    }
    
    func supportButtonTapped(indexPath : IndexPath)
    {
        let user_id = UserDefaults.standard.string(forKey: "user_id")
        if (user_id == nil) || (user_id?.isEmpty)!
        {
            self.alertMessage(title: "Alert", message:"Please login first!")
        }else{
            
            
            // alertMessage(title: "Alert!", message: "Go to device settings to cancel your subcription plan.")
            
            let alertController = UIAlertController.init(title: "Remove!", message: "Are you sure you want to remove subscription.", preferredStyle: .alert)
            let alertAction = UIAlertAction.init(title: "Yes", style: .default) { (action) in
                let Shopodcast_id = (self.arraySuppored.object(at: indexPath.row) as! NSDictionary).value(forKey: "show_cat") as! String
                let checpost_id = UserDefaults.standard.string(forKey: "user_id")
                let URL = "\(appDelegate.common.Obj.base_url)user_Unsupported.php"
                let params = ["user_id": checpost_id,
                              "show_cat": Shopodcast_id
                ]
                let manager = AFHTTPSessionManager()
                manager.responseSerializer = AFJSONResponseSerializer()
                manager.post(URL, parameters: params, constructingBodyWith: { (form) in
                    
                }, progress: { (progress) in
                    
                }, success: { (operation, responseObject ) -> Void in
                    let dict = responseObject as! NSDictionary
                    let code =  (dict.object(forKey: "response") as! NSDictionary).value(forKey: "code") as! String
                    if code == "200"{
                        self.arraySuppored.removeObject(at: indexPath.row)
                        self.tblOrganisation.reloadData()
                        self.viewWillAppear(true)
                    }
                    
                }) { (operation, error) -> Void in
                    print(error)
                    //self.alertMessage(title: "Alert", message:"Somthing is wrong!")
                    
                }
            }
            let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(alertAction)
            alertController.addAction(alertActionCancel)
            self.present(alertController, animated: true, completion: nil)
            
        }
        
        
    }
    //    @objc func tapaccessoryButton(sender:UITapGestureRecognizer) {
    //        let tag = sender.view?.tag
    //        let indexpath = IndexPath(row: tag!, section: 0)
    //        self.tableView(tblOrganisation, accessoryButtonTappedForRowWith: indexpath)
    //    }
    
    //    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    //    {
    //        return true
    //    }
    //action
    
    
    //    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
    //        let user_id = UserDefaults.standard.string(forKey: "user_id")
    //        if (user_id == nil) || (user_id?.isEmpty)!
    //        {
    //            self.alertMessage(title: "Alert", message:"Please login first!")
    //        }else{
    //
    //            let alertController = UIAlertController.init(title: "Unsubscribe!", message: "Are you sure you want to unsubscribe this show.", preferredStyle: .alert)
    //            let alertAction = UIAlertAction.init(title: "Yes", style: .default) { (action) in
    //                let Shopodcast_id = (self.myarray.object(at: indexPath.row) as! NSDictionary).value(forKey: "show_cat") as! String
    //                let checpost_id = UserDefaults.standard.string(forKey: "user_id")
    //                let URL = "\(appDelegate.common.Obj.base_url)user_Unsubscribe.php"
    //                let params = ["user_id": checpost_id,
    //                              "show_cat": Shopodcast_id
    //                ]
    //
    //                let manager = AFHTTPSessionManager()
    //                manager.responseSerializer = AFJSONResponseSerializer()
    //
    //                manager.post(URL, parameters: params, constructingBodyWith: { (form) in
    //
    //                }, progress: { (progress) in
    //
    //                }, success: { (operation, responseObject ) -> Void in
    //                    let dict = responseObject as! NSDictionary
    //                    let code =  (dict.object(forKey: "response") as! NSDictionary).value(forKey: "code") as! String
    //                    if code == "200"{
    //                        self.myarray.removeObject(at: indexPath.row)
    //                        //                    let url = thedownloads[indexPath.row]
    //                        //                    print(url)
    //                        //
    //                        //                    do {
    //                        //                        try FileManager.default.removeItem(at: theurls[indexPath.row])
    //                        //                        // self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
    //                        //                        // self.tableView.reloadData()
    //                        //                    } catch let error {
    //                        //                        print(error)
    //                        //                    }
    //                        self.tblOrganisation.reloadData()
    //                    }
    //
    //                }) { (operation, error) -> Void in
    //                    print(error)
    //                    //self.alertMessage(title: "Alert", message:"Somthing is wrong!")
    //
    //                }
    //            }
    //            let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
    //            alertController.addAction(alertAction)
    //            alertController.addAction(alertActionCancel)
    //            self.present(alertController, animated: true, completion: nil)
    //
    //
    //        }
    //    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let epi = TableDataV[indexPath.row]
        var url = ""
        let loginSes = userDef.value(forKey: "session")
        if(loginSes == nil){
            let alert = UIAlertController(title: "Alert", message: "Please login first!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    self.navigationController?.pushViewController(login, animated: true)
                case .cancel:
                    print("cancel")
                case .destructive:
                    print("jfhjfh")
                }}))
            self.present(alert, animated: true, completion: nil)
            return
        }
        else
        {
            
            if epi.audio != nil {
                url = epi.audio!
                print(url)
                if let audioUrl = URL(string: url)
                {
                    
                    // then lets create your document folder url
                    let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                    
                    // lets create your destination file url
                    let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
                    
                    if FileManager.default.fileExists(atPath: destinationUrl.path) {
                        
                        print("The file already exists at path")
                        
                        audioPlayer = nil
                        
                        let when = DispatchTime.now() + 0.5
                        
                        DispatchQueue.main.asyncAfter(deadline: when) {
                            
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EpisodeViewController") as! EpisodeViewController
                            self.navigationController?.pushViewController(vc, animated: true)
                            nameVariableInSecondVc = epi.name
                            
                            
                            //                                        let uuu = URL(string: epi.audio!)
                            //                                        let newUrl = documentsDirectoryURL.appendingPathComponent((uuu?.lastPathComponent)!)
                            //                                        audioVariableInSecondVc = newUrl.absoluteString
                            
                            audioVariableInSecondVc =  epi.audio!
                            showTitleVariable = self.showNameVariable
                            descriptionVariable = epi.description
                            imageVariable = epi.image!
                            showDateVariable = epi.date
                            
                            //      let str = Bundle.main.path(forResource: "430824201-user-774038550-tulane-spring-game-review-ep-28", ofType: "m4a")
                            // nameVariableInSecondVc = "Local"
                            //    audioVariableInSecondVc = str!
                        }
                        
                        
                        // if the file doesn't exist
                    } else {
                        
                        activity_indicator.isHidden = false
                        activity_indicator.startAnimating()
                        UIApplication.shared.beginIgnoringInteractionEvents()
                        
                        // you can use NSURLSession.sharedSession to download the data asynchronously
                        URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                            guard let location = location, error == nil else {
                                
                                return
                                
                            }
                            do {
                                // after downloading your file you need to move it to your destination url
                                try FileManager.default.moveItem(at: location, to: destinationUrl)
                                print("File moved to documents folder")
                                
                                
                                let when = DispatchTime.now() + 0.5
                                
                                DispatchQueue.main.asyncAfter(deadline: when) {
                                    
                                    self.activity_indicator.isHidden = true
                                    self.activity_indicator.stopAnimating()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                    
                                    
                                    ApiResponse.alert(title: "Success", message: "Download Completed.", controller: self)
                                    self.tblOrganisation.reloadData()
                                    
                                    print("audio:\(audioVariableInSecondVc)")
                                    
                                }
                                
                                
                            } catch let error as NSError {
                                print(error.localizedDescription)
                            }
                        }).resume()
                    }
                    
                }
            }
        }
        //  else
        //            {
        //                // if (epi.tblname == "1") && (epi.supportStatus == "0")
        ////                {
        ////                    if(epi.paywall == "yes")
        ////                    {
        ////                        alertController(message: "You must support this show to access.", alert: "Alert!")
        ////                        return
        ////                    }
        ////                    else
        ////                    {
        ////                        if epi.audio != nil {
        ////                            //&& (epi.paywall == "yes")
        ////                            url = epi.audio!
        ////                            let status = epi.paywall
        ////                            //                if (epi.tblname == "0")  {
        ////                            //                    self.alertController(message: "Must support this show to access!", alert: "Alert")
        ////                            //                    return
        ////                            //                }
        ////                            //else
        ////                            //  {
        ////                            //                    if (epi.paywall == "yes") {
        ////                            //                        self.alertController(message: "Must support this show to access!", alert: "Alert")
        ////                            //                        return
        ////                            //                    }
        ////                            if let audioUrl = URL(string: url)
        ////                            {
        ////
        ////                                // then lets create your document folder url
        ////                                let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        ////
        ////                                // lets create your destination file url
        ////                                let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
        ////
        ////                                if FileManager.default.fileExists(atPath: destinationUrl.path) {
        ////
        ////                                    print("The file already exists at path")
        ////
        ////                                    audioPlayer = nil
        ////
        ////                                    let when = DispatchTime.now() + 0.5
        ////
        ////                                    DispatchQueue.main.asyncAfter(deadline: when) {
        ////
        ////                                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EpisodeViewController") as! EpisodeViewController
        ////                                        self.navigationController?.pushViewController(vc, animated: true)
        ////                                        nameVariableInSecondVc = epi.name
        ////                                        audioVariableInSecondVc = epi.audio!
        ////                                        showTitleVariable = self.showNameVariable
        ////                                        descriptionVariable = epi.description
        ////                                        imageVariable = epi.image!
        ////                                        showDateVariable = epi.date
        ////                                    }
        ////
        ////
        ////                                    // if the file doesn't exist
        ////                                } else {
        ////
        ////                                    activity_indicator.isHidden = false
        ////                                    activity_indicator.startAnimating()
        ////                                    UIApplication.shared.beginIgnoringInteractionEvents()
        ////
        ////                                    // you can use NSURLSession.sharedSession to download the data asynchronously
        ////                                    URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
        ////                                        guard let location = location, error == nil else {
        ////
        ////                                            return
        ////
        ////                                        }
        ////                                        do {
        ////                                            // after downloading your file you need to move it to your destination url
        ////                                            try FileManager.default.moveItem(at: location, to: destinationUrl)
        ////                                            print("File moved to documents folder")
        ////
        ////
        ////                                            let when = DispatchTime.now() + 0.5
        ////
        ////                                            DispatchQueue.main.asyncAfter(deadline: when) {
        ////
        ////                                                self.activity_indicator.isHidden = true
        ////                                                self.activity_indicator.stopAnimating()
        ////                                                UIApplication.shared.endIgnoringInteractionEvents()
        ////
        ////                                                self.ApiDownloadHistory(show_cat: self.show_cat)
        ////                                                let cell = tableView.cellForRow(at: indexPath) as! FirstViewCell
        ////                                                self.alertController(message: "Success", alert: "Download Completed.")
        ////                                                self.episodeTable.reloadData()
        ////                                                //cell.img_status.image = #imageLiteral(resourceName: "verification-mark")
        ////                                                //kndl complete
        ////                                                //cell.view_complete.isHidden = false
        ////                                                //                                audioPlayer = nil
        ////                                                //
        ////                                                //                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "EpisodeViewController") as! EpisodeViewController
        ////                                                //                                self.navigationController?.pushViewController(vc, animated: true)
        ////                                                //                                nameVariableInSecondVc = epi.name
        ////                                                //                                audioVariableInSecondVc = epi.audio!
        ////                                                //                                showTitleVariable = self.showNameVariable
        ////                                                //                                imageVariable = epi.image!
        ////
        ////                                                print("audio:\(audioVariableInSecondVc)")
        ////
        ////                                            }
        ////
        ////
        ////                                        } catch let error as NSError {
        ////                                            print(error.localizedDescription)
        ////                                        }
        ////                                    }).resume()
        ////                                }
        ////
        ////                            }
        ////                            //  }
        ////
        ////
        ////                        }
        ////                    }
        ////                }
        //
        //            }
        
        
    }
    
    func alertMessage(title : String , message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default , handler: nil)
        alertController.addAction(alertAction)
        self.present(alertController, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //@IBOutlet var subscribeTable: UITableView!
    
}

