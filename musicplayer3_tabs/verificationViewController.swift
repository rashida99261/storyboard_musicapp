//
//  verificationViewController.swift
//  musicplayer3_tabs
//
//  Created by mac on 20/11/18.
//

import UIKit

class verificationViewController: UIViewController {
    
    @IBOutlet weak var VerficationCode: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func clickOnBackBtn(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickOnVerifyBtn(_ sender : UIButton){
     //   emailField.text = "julee.b@westcoastinfotech.com"
    //    VerficationCode.text = "0f788d5c70ca95b6784e"
     //   passField.text = "123456"
        
        
        if emailField?.text != "" && passField.text != "" && VerficationCode.text != ""{
            
            let params = ["email": emailField.text!,
                          "password": passField.text!,
                          "verification_code": VerficationCode.text!]
                as NSDictionary
            
            self.callApiForResponse(url: Constant.verifyUrl, param: params, strCheck: "verify")
        }
        else{
            ApiResponse.alert(title: "Alert", message: "Please fill the form.", controller: self)
        }
    }
    
    func callApiForResponse(url : String , param : NSDictionary , strCheck : String){
        ApiResponse.onResponsePost(url: url, parms: param) { (dict, error) in
            if(error == ""){
                if(strCheck == "verify"){
                    OperationQueue.main.addOperation {
                        let userData = dict["data"] as! NSDictionary
                        let encodedData = NSKeyedArchiver.archivedData(withRootObject: userData)
                        userDef.set(encodedData, forKey: "userData")
                        userDef.set("login", forKey: "session")
                        
                        
                        let alertController = UIAlertController.init(title: "Alert", message: "Your account is verified successfully.", preferredStyle: .alert)
                        let alertAction = UIAlertAction.init(title: "OK", style: .default) { (action) in
                            
                            
                        let login = self.storyboard?.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
                            self.navigationController?.pushViewController(login, animated: true)
                            
                        }
                        alertController.addAction(alertAction)
                        self.present(alertController, animated: true, completion: nil)
                        
                        
                        
                    }
                }
            }
            else{
                ApiResponse.alert(title: "Alert", message: "Your information does not match any Storyboard account for this organization. Please double check that your email and verification code are correct and resubmit", controller: self)
            }
        }
    }
}

extension verificationViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField === emailField){
            emailField.resignFirstResponder()
            passField.becomeFirstResponder()
        }
        if (textField === passField){
            emailField.resignFirstResponder()
            VerficationCode.becomeFirstResponder()
        }
        
        textField.resignFirstResponder()
        return true
    }
}
