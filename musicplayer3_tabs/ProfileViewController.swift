//
//  ProfileViewController.swift
//  musicplayer3_tabs
//
//  Created by Kindlebit on 18/06/18.
//  Copyright © 2018 storyboard. All rights reserved.
//

import UIKit
import SwiftyStoreKit
import AFNetworking

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var lbl_first_name : UILabel!
    @IBOutlet weak var lbl_email : UILabel!
    @IBOutlet weak var lbl_username : UILabel!
    @IBOutlet weak var switchPhus : UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.UserSettingPush()
        switchPhus.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        
        let outData = userDef.data(forKey: "userData")
        let dict = NSKeyedUnarchiver.unarchiveObject(with: outData!)as! NSDictionary
        
        print(dict)
        
        self.lbl_first_name.text = dict["first_name"] as? String
        self.lbl_email.text = dict["email"] as? String
        self.lbl_username.text = dict["uid"] as? String   //username
        
        // Do any additional setup after loading the view.
    }
    @IBAction func UserSettingPush()
    {
        if(UserDefaults.standard.value(forKey: "user_id") != nil){
            if(UserDefaults.standard.object(forKey: "pushnotification") != nil){
                if(UserDefaults.standard.value(forKey: "pushnotification") as! String == "1"){
                    switchPhus.isOn = true
                } else {
                    switchPhus.isOn = false
                }
            }
        } else {
            switchPhus.isOn = false
        }
    }
    @IBAction func ActionSwitchPush(_ sender: UISwitch) {
        if (sender.isOn == true){
            print("UISwitch state is now ON")
            self.ApiUpdateSetting(pushnotify: "1")
        }
        else{
            print("UISwitch state is now Off")
            self.ApiUpdateSetting(pushnotify: "0")
        }
    }
    func ApiUpdateSetting(pushnotify : String) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let URL = "\(appDelegate.common.Obj.base_url)updatePush.php"
        let params = ["user_id": UserDefaults.standard.value(forKey: "user_id"),
                      "pushnotification": pushnotify
        ]
        print(params)
        
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFJSONResponseSerializer()
        
        manager.post(URL, parameters: params, constructingBodyWith: { (form) in
            
        }, progress: { (progress) in
            
        }, success: { (operation, responseObject ) -> Void in
            
            let dict = responseObject as! NSDictionary
            print(dict)
            let code =  (dict.object(forKey: "response") as! NSDictionary).value(forKey: "code") as! String
            if code == "200"{
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                UserDefaults.standard.set(pushnotify, forKey: "pushnotification")
                UserDefaults.standard.synchronize()
            }else{
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                //self.alertController(message: (dict.object(forKey: "response") as! NSDictionary).value(forKey: "msg") as! String, alert: "Alert")
            }
            
        }) { (operation, error) -> Void in
            print(error)
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            //self.alertController(message: "Something is wrong!", alert: "Alert")
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.UserSettingPush()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func ActionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ActionLogout(_ sender: UIButton) {
        userDef.removeObject(forKey: "session")
        userDef.removeObject(forKey: "userData")
        userDef.removeObject(forKey: "organization_id")
        isNAvigateFrom = ""
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func restoreButtonPressed(_ sender: UIButton)
    {
        SwiftyStoreKit.restorePurchases(atomically: true) { results in
            if results.restoreFailedPurchases.count > 0 {
                print("Restore Failed: \(results.restoreFailedPurchases)")
            }
            else if results.restoredPurchases.count > 0 {
                print("Restore Success: \(results.restoredPurchases)")
            }
            else {
                print("Nothing to Restore")
            }
        }
    }

}
